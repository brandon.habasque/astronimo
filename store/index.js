export const state = () => ({
  starsList: []
})

export const mutations = {
  // Ajoute les astres à la liste 
  add(state, stars) {
    state.starsList = stars;
  },
}

export const getters = {
  // Permet de récupérer les astres
  getStars: (state) => {
    return state.starsList;
  }
}