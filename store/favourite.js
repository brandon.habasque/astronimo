export const state = () => ({
    favouriteList: []
})

export const mutations = {
    // Ajoute un star à la liste des favoris 
    add(state, star) {
        state.favouriteList.push(star);
    },
    // Retire un star de la liste des favoris
    remove(state, star) {
        state.favouriteList.splice(state.favouriteList.indexOf(star), 1);
    }
}

export const getters = {
    // Permet de récupérer les favoris
    getFavourites: (state) => {
        return state.favouriteList;
    }
}