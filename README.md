# Astronimo

Astronimo est un projet destiné à l'utilisation du Framework Vue JS dans le cadre de ma formation au MBA développeur full-stack

Il consiste à afficher une liste d'astres, pouvoir afficher les détails d'un astre et avoir une liste de favoris

## Installation

Cloner ce projet puis :

```bash
npm install
```

## Usage

Pour lancer le projet dans le navigateur :
```bash
npm run dev
```

## Statut

Ce projet est terminé

## Bonus

Pour ce TP j'ai mis en place un plugin de tooltip